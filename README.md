# Digital Media

This repository houses digital media (i.e. images) from Troll Lord Games used for various sites such as the product pages on FoundryVTT, etc.

## Folders

* foundryvtt-cnc-system
    * Content for the FoundryVTT Castles & Crusades system
* foundryvtt-cnc-quickstart
    * Content for the FoundryVTT Castles & Crusades quickstart module


## Copyright

Copyright 2023, Troll Lord Games.

